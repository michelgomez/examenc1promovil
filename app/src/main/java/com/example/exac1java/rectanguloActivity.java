package com.example.exac1java;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.content.DialogInterface;

public class rectanguloActivity extends AppCompatActivity {
    private TextView lblBase;
    private TextView lblAltura;
    private EditText txtBase;
    private EditText txtAltura;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private TextView lblCalArea;
    private TextView txtCalcularArea;
    private TextView lblCalPerimetro;
    private TextView txtCalcularPerimetro;
    private EditText txtNombre;
    private Rectangulo rectangulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);

        iniciarComponentes();

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcular();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });

        rectangulo = new Rectangulo(0f, 0f);
    }

    private void iniciarComponentes() {
        lblBase = findViewById(R.id.lblBase);
        lblAltura = findViewById(R.id.lblAltura);
        txtBase = findViewById(R.id.txtBase);
        txtAltura = findViewById(R.id.txtAltura);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        lblCalArea = findViewById(R.id.lblCalArea);
        txtCalcularArea = findViewById(R.id.txtCalcularArea);
        lblCalPerimetro = findViewById(R.id.lblCalPerimetro);
        txtCalcularPerimetro = findViewById(R.id.txtCalcularPerimetro);
        txtNombre = findViewById(R.id.txtNombre);
    }

    private void calcular() {
        String base = txtBase.getText().toString().trim();
        String altura = txtAltura.getText().toString().trim();

        if (!base.isEmpty() && !altura.isEmpty()) {
            float baseR = Float.parseFloat(base);
            float alturaR = Float.parseFloat(altura);

            if (baseR > 0 && alturaR > 0) {
                rectangulo.setBase(baseR);
                rectangulo.setAltura(alturaR);
                txtCalcularArea.setText(String.valueOf(rectangulo.calcularArea()));
                txtCalcularPerimetro.setText(String.valueOf(rectangulo.calcularPerimetro()));
            } else {
                Toast.makeText(this, "Ambos números deben ser positivos", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Ingrese todos los campos", Toast.LENGTH_SHORT).show();
        }
    }

    private void limpiar() {
        txtBase.setText("");
        txtAltura.setText("");
        txtCalcularArea.setText("");
        txtCalcularPerimetro.setText("");
    }

    private void regresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Rectangulo");
        confirmar.setMessage("¿Desea regresar?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        confirmar.setNegativeButton("Cancelar", null);
        confirmar.show();
    }
}
